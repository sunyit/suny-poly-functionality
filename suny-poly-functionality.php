<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wasielewski.org
 * @since             0.1.0
 * @package           Suny_Poly_Functionality
 *
 * @wordpress-plugin
 * Plugin Name:       SUNY Poly Functionality
 * Plugin URI:        https://bitbucket.org/sunyit/suny-poly-functionality
 * Description:       Functionality required by the SUNY Poly WordPress theme, notably: redirecting nonexistent URLs to the legacy WebAce CMS.
 * Version:           0.2.1
 * Author:            Zac Wasielewski
 * Author URI:        http://wasielewski.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       suny-poly-functionality
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-suny-poly-functionality-activator.php
 */
function activate_suny_poly_functionality() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-suny-poly-functionality-activator.php';
	Suny_Poly_Functionality_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-suny-poly-functionality-deactivator.php
 */
function deactivate_suny_poly_functionality() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-suny-poly-functionality-deactivator.php';
	Suny_Poly_Functionality_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_suny_poly_functionality' );
register_deactivation_hook( __FILE__, 'deactivate_suny_poly_functionality' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-suny-poly-functionality.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_suny_poly_functionality() {

	$plugin = new Suny_Poly_Functionality();
	$plugin->run();

}
run_suny_poly_functionality();
