<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/admin
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Suny_Poly_Functionality_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suny_Poly_Functionality_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suny_Poly_Functionality_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/suny-poly-functionality-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suny_Poly_Functionality_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suny_Poly_Functionality_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/suny-poly-functionality-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function register_admin_menu() {

    add_options_page(__('Theme Settings','suny-poly-functionality'), __('SUNY Poly Theme','suny-poly-functionality'), 'manage_options', 'suny-poly-functionality', array( $this, 'admin_settings_page' ) );
	
	}
	
	public function register_admin_settings() {
	  
    register_setting( 'suny-poly-functionality-settings', 'suny_poly_theme_menu_html' );

    // set default values
    add_option( 'suny_poly_theme_menu_html', '' );

	}

  public function admin_settings_page() {
    $html = $this->get_partial( 'suny-poly-functionality-admin-settings.php', array( 'plugin_admin' => $this ) );
    echo $html;
  }

	private function get_partial( $file, $variables = '' ) {
		$path = plugin_dir_path( __FILE__ ) . 'partials/' . $file ;
	  $partial = get_include_contents( $path, $variables );
	  return $partial;
	}	

  public function gf_limit_entries_by_field_value_setting($settings, $form) {
    $settings['Restrictions']['limit_entries_by_field_value'] = '
      <tr>
        <th><label for="limit_entries_by_field_value">Limit entries by field value</label></th>
        <td><textarea name="limit_entries_by_field_value">' . rgar($form, 'limit_entries_by_field_value') . '</textarea></td>
      </tr>';

    return $settings;
  }

  // save your custom form setting
  public function gf_save_limit_entries_by_field_value_setting($form) {
    $form['limit_entries_by_field_value'] = rgpost('limit_entries_by_field_value');
    return $form;
  }

}
