<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">

  <h2><?php echo __( 'SUNY Poly Theme Settings', 'suny-poly-functionality' ) ?></h2>

  <form method="post" action="options.php">
    <?php settings_fields( 'suny-poly-functionality-settings' ); ?>
    <?php do_settings_sections( 'suny-poly-functionality-settings' ); ?>
    
    <h3 class="title">Main Menu HTML</h3>
    <p><textarea class="widefat" rows="25" cols="4" name="suny_poly_theme_menu_html" class="inside"><?php echo esc_textarea( get_option('suny_poly_theme_menu_html') ); ?></textarea></p>
    <p><?php echo __( 'Markup should follow the <a href="http://geedmo.github.io/yamm3/" target="_blank">Yamm 3</a> plugin.'); ?></p>

    <?php submit_button(); ?>    
  </form>

</div>
