<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/public
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Suny_Poly_Functionality_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suny_Poly_Functionality_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suny_Poly_Functionality_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/suny-poly-functionality-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suny_Poly_Functionality_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suny_Poly_Functionality_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/suny-poly-functionality-public.js', array( 'jquery' ), $this->version, false );

	}
	
	public function redirect_404() {
	  if (is_404()) {
	    $this->redirect_with_query();
	  }
	}
	
	private function redirect_with_query() {

    $url_parts = parse_url($_SERVER['REQUEST_URI']);
    
    $query_args = array();
    if ( isset($url_parts['query']) ) {
      parse_str( $url_parts['query'], $query_args );
    }
	  $query = array_merge( $query_args, array( 'cms' => 'webace' ) );
	  
	  $redirect = http_build_url(
	    $_SERVER['REQUEST_URI'],
	    array(
	      'query' => http_build_query($query)
	    )
	  );
	  
    header('Location: ' . $redirect);
    exit;
	}

  private function no_redirect_on_404($redirect_url) {
    if (is_404()) {
      return false;
    }
    return $redirect_url;
  }
  
  public function init_gravity_forms_inventory($form) {
    
    $json = rgar($form, 'limit_entries_by_field_value');
    $settings = json_decode($json);
    if ($settings === NULL) { return $form; }
    
    foreach ($settings as $field_id => $limits) {
      foreach ($limits as $value => $limit) {
        
        new GravityFormsInventory(array(
          'form_id'  => intval($form['id']),
          'input_id' => intval($field_id),
          'option'   => $value,
          'limit'    => intval($limit)
        ));
        
      }
    }
    
    return $form;
  }
  
}
