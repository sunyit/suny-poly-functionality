<?php

/**
 * Fired during plugin activation
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/includes
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Suny_Poly_Functionality_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
