<?php

class GravityFormsInventory {

  private $_args;

  function __construct($args) {

    $this->_args = wp_parse_args($args, array(
      'form_id' => false,
      'input_id' => false,
      'option' => false,
      'options' => array(),
      'limit' => null,
      'limit_message' => __('Sorry, this item is sold out.'),
      'validation_message' => __('You selected %1$s of this item. There are only %2$s of this item left.'),
      'approved_payments_only' => false,
      'hide_form' => false
    ));

    add_filter('gform_field_content', array(&$this, 'disable_unavailable_options' ), 10, 5);

  }

  public function disable_unavailable_options( $field_content, $field, $value, $entry_id, $form_id ) {
    
    $option_value = $this->_args['option'];
    
    if ($field['id'] !== $this->_args['input_id']) {
      return $field_content;
    }
    if ($this->is_option_available( $form_id, $field['id'], $option_value ) === TRUE) {
      return $field_content;
    }
    
    $search  = sprintf("<option value='%s'",$option_value);
    $replace = $search . ' disabled';
    $field_content = str_replace( $search, $replace, $field_content );

    $search  = sprintf(">%s<",$option_value);
    $replace = sprintf(">%s - UNAVAILABLE<",$option_value);
    $field_content = str_replace( $search, $replace, $field_content );
    
    return $field_content;
  }
  
  private function is_option_available( $form_id, $input_id, $value ) {
    $limit = $this->_args['limit'];
    $num_entries = $this->get_sum_of_entries_with_field_value( $form_id, $input_id, $value );
    return ($num_entries < $limit);    
  }

  public static function get_sum_of_entries_with_field_value($form_id, $input_id, $value) {
    global $wpdb;

    $query = apply_filters( 'gwlimitbysum_query', array(
      'select' => 'SELECT COUNT(*)',
      'from' => "FROM {$wpdb->prefix}rg_lead_detail ld",
      'join' => '',
      'where' => $wpdb->prepare( "WHERE ld.form_id = %d AND CAST( ld.field_number as unsigned ) = %d AND ld.value = '%s'", $form_id, $input_id, $value )
    ), $form_id, $input_id );
    
    $wpdb->show_errors();

    $sql = implode( ' ', $query );
    $result = $wpdb->get_var( $sql );

    return intval( $result );
  }
  
  /*
	public function shortcode_remaining( $output, $atts ) {

		$atts = shortcode_atts( array(
			'id' => false,
			'input_id' => false,
			'limit'    => false
		), $atts );

		extract( $atts ); // gives us $id, $input_id

		$remaining = $limit - intval( self::get_field_values_sum( $id, $input_id ) );

		return max( 0, $remaining );
	}
	*/

}
