<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Suny_Poly_Functionality
 * @subpackage Suny_Poly_Functionality/includes
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Suny_Poly_Functionality_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
