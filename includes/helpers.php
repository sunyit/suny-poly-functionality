<?php

function get_include_contents( $filename, $variables = null ) {
  if (is_array($variables)) {
    extract($variables); // accept key => value array to be used as variables in include file
  }
  if (is_file( $filename )) {
    ob_start();
    include $filename;
    return ob_get_clean();
  }
  return false;
}
